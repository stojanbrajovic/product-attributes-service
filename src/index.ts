import express from "express";
import { port } from "../config.json";
import { logger } from "./logger";
import { initializeCrudRoutes } from "./routes/initializeCrudRoutes";
import { attributeCrud } from "./mongodb/attribute";
import { categoriesCrud } from "./mongodb/category";
import { offersCrud } from "./mongodb/offer";
import { addVerification } from "./verification/crudWithVerification";
import { verifyAttribute } from "./verification/attributesVerification.js";

// Parse JSON bodies (as sent by API clients)
const app = express();
app.use(express.json());

initializeCrudRoutes(
	"attributes",
	app,
	addVerification(attributeCrud, verifyAttribute),
);
initializeCrudRoutes("categories", app, categoriesCrud);
initializeCrudRoutes("offers", app, offersCrud);

app.listen(port, () => logger.info(`App listening on port ${port}!`));
