import { StoreObject } from "../types/StoreObject";

const NAME_DOESNT_EXIST = "Name must be present";
const MIN_NAME_LENGTH = 3;
const NAME_TOO_SHORT = `Name must be longer than ${MIN_NAME_LENGTH} characters`;

const MIN_DESCRIPTION_LENGTH = 10;
const DESCRIPTION_TOO_SHORT = `Description must be longer than ${MIN_DESCRIPTION_LENGTH} characters`;

const LANGUAGE_NOT_PRESENT = "Language code must be provided";
const MIN_LANGUAGE_CODE_LENGTH = 2;
const LANGUAGE_TOO_SHORT = `Language code must be longer than ${MIN_LANGUAGE_CODE_LENGTH} characters`;

export const verifyStoreObject = (obj: StoreObject) => {
	const { name, description, languageCode } = obj;
	if (!name) {
		throw new Error(NAME_DOESNT_EXIST);
	}
	if (name.length < MIN_NAME_LENGTH) {
		throw new Error(`${NAME_TOO_SHORT}; ${name}`);
	}
	if (description && description.length < MIN_DESCRIPTION_LENGTH) {
		throw new Error(`${DESCRIPTION_TOO_SHORT}; ${description}`);
	}
	if (!languageCode) {
		throw new Error(LANGUAGE_NOT_PRESENT);
	}
	if (languageCode.length < MIN_LANGUAGE_CODE_LENGTH) {
		throw new Error(`${LANGUAGE_TOO_SHORT}; ${languageCode}`);
	}
};
