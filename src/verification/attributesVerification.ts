import {
	Attribute,
	AttributeType,
	NumericAttribute,
	TextAttribute,
} from "../types/Attribute";
import { verifyStoreObject } from "./storeObjectVerification";

const MIN_MUST_BE_SMALLER_THAN_MAX = "Minimum must be smaller than maximum";
const STEP_TOO_LARGE = "Step is too large";
const STEP_MUST_BE_POSITIVE = "Step must be a positive value";

const verifyNumericAttribute = (attr: NumericAttribute) => {
	const min = attr.min === 0 || attr.min ? attr.min : Number.MIN_SAFE_INTEGER;
	const max = attr.max === 0 || attr.max ? attr.max : Number.MAX_SAFE_INTEGER;
	if (min >= max) {
		throw new Error(MIN_MUST_BE_SMALLER_THAN_MAX);
	}
	const step = attr.step || 0;
	if (min + step > max) {
		throw new Error(STEP_TOO_LARGE);
	}
	if (step <= 0) {
		throw new Error(STEP_MUST_BE_POSITIVE);
	}
};

const VALUES_MUST_BE_ARRAY = "Predefined values - not an array";

const verufyTextAttribute = (attr: TextAttribute) => {
	const values = attr.predefinedValues || [];
	if (!Array.isArray(values)) {
		throw new Error(VALUES_MUST_BE_ARRAY);
	}
};

export const verifyAttribute = (attr: Attribute) => {
	verifyStoreObject(attr);

	if (attr.type === AttributeType.NUMERIC) {
		verifyNumericAttribute(attr);
	}

	if (attr.type === AttributeType.TEXT) {
		verufyTextAttribute(attr);
	}
};
