import { StoreObject } from "../types/StoreObject";
import { CrudOperations } from "../mongodb/createCrudOperations";

const ID_NOT_ALLOWED_WHEN_CREATING = "Objects cannot be created with _id";
const CANNOT_UPDATE_WITHOUT_ID = "Cannot update object without id";

export const addVerification = <DataType extends StoreObject>(
	crudOperations: CrudOperations<DataType>,
	verifyData: (data: DataType) => void,
) => {
	const result = { ...crudOperations };
	const { create, update } = result;
	result.create = (data: DataType[]) => {
		data.forEach(d => {
			if (d._id) {
				throw new Error(ID_NOT_ALLOWED_WHEN_CREATING);
			}
			verifyData(d);
		});
		return create(data);
	};
	result.update = (data: DataType[]) => {
		data.forEach(d => {
			if (!d._id) {
				throw new Error(CANNOT_UPDATE_WITHOUT_ID);
			}
			verifyData(d);
		});
		return update(data);
	};

	return result;
};
