import { createLogger, format, transports } from "winston";
import "winston-daily-rotate-file";
import { logsDir } from "../config.json";

const dailyTransportConfig = {
	datePattern: "YYYY-MM-DD-HH",
	zippedArchive: true,
	maxSize: "20m",
	maxFiles: "60d",
};

export const logger = createLogger({
	level: "info",
	format: format.combine(
		format.timestamp({
			format: "YYYY-MM-DD HH:mm:ss",
		}),
		format.errors({ stack: true }),
		format.splat(),
		format.json(),
	),
	defaultMeta: { service: "users-service" },
	transports: [
		new transports.DailyRotateFile({
			...dailyTransportConfig,
			filename: `${logsDir}/%DATE%-error.log`,
			level: "error",
		}),
		new transports.DailyRotateFile({
			...dailyTransportConfig,
			filename: `${logsDir}/%DATE%-combined.log`,
		}),
	],
});
