import express, { Request, Response } from "express";
import { CrudOperations } from "../mongodb/createCrudOperations";
import { logger } from "../logger";
import { StoreObject } from "../types/StoreObject";

const INTERNAL_SERVER_ERROR_STATUS = 500;

const createRouteHandler = <Argument, Result>(
	handler: (data: Argument) => Promise<Result>,
) => async (req: Request, res: Response) => {
	const requestInfo = {
		path: req.route.path,
		body: JSON.stringify(req.body),
		query: JSON.stringify(req.query),
		headers: JSON.stringify(req.headers),
	};

	logger.info("Received request", requestInfo);

	try {
		const argument: Argument = req.body || req.query;
		const result: Result = await handler(argument);
		logger.info("Response sent", {
			...requestInfo,
			result,
		});

		res.send(result);
	} catch (error) {
		logger.error(error, requestInfo);

		res.status(INTERNAL_SERVER_ERROR_STATUS).send({
			error: {
				message: (error && error.message) || "Server error",
			},
		});
	}
};

export const initializeCrudRoutes = <DataType extends StoreObject>(
	url: string,
	app: ReturnType<typeof express>,
	crudObj: CrudOperations<DataType>,
) => {
	app.post(`/${url}`, createRouteHandler(crudObj.create));
	app.get(`/${url}`, createRouteHandler(crudObj.read));
	app.patch(`/${url}`, createRouteHandler(crudObj.update));
	app.delete(`/${url}`, createRouteHandler(crudObj.delete));
};
