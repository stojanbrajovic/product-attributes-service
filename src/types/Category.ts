import { ObjectID } from "mongodb";
import { StoreObject } from "./StoreObject";
import { AttributeValue } from "./AttributeValue";

type Category = StoreObject & {
	childrenCategoryIds?: ObjectID[];
	attributes?: AttributeValue[];
	storeId?: ObjectID;
};

export default Category;
