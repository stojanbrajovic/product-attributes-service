import { StoreObject } from "./StoreObject";

export enum AttributeType {
	BOOLEAN = "BOOLEAN",
	NUMERIC = "NUMERIC",
	TEXT = "TEXT",
}

type AbstractAttribute = StoreObject & {
	type: AttributeType;
};

export type BooleanAttribute = AbstractAttribute & {
	type: AttributeType.BOOLEAN;
};

export type NumericAttribute = AbstractAttribute & {
	max?: number;
	min?: number;
	step?: number;
	type: AttributeType.NUMERIC;
};

export type TextAttribute = AbstractAttribute & {
	type: AttributeType.TEXT;
	predefinedValues: string[];
};

export type Attribute = BooleanAttribute | NumericAttribute | TextAttribute;
