import { ObjectID } from "mongodb";

export type StoreObject = {
	_id?: ObjectID;
	name: string;
	description?: string;
	languageCode: string;
};
