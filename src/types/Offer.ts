import { ObjectID } from "bson";
import { StoreObject } from "./StoreObject";
import { AttributeValue } from "./AttributeValue";

export type Offer = StoreObject & {
	attributes: AttributeValue[];
	categoryIds: ObjectID[];
	storeId: ObjectID;
	price: number;
	currency: string;
	quantity?: number;
};
