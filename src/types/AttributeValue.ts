import { ObjectID } from "bson";

export type AttributeValue = {
	attributeId: ObjectID;
	value?: number | string | boolean;
};
