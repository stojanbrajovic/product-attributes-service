import { getDb } from "./getDb";
import { createCrudOperations } from "./createCrudOperations";
import { Offer } from "../types/Offer";

const OFFER_COLLECTION = "OFFER_COLLECTION";

const getCollection = async () => {
	const db = await getDb();
	return db.collection(OFFER_COLLECTION);
};

export const offersCrud = createCrudOperations<Offer>(getCollection);
