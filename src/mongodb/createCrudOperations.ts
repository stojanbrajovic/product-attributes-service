import {
	Collection,
	ObjectID,
	InsertWriteOpResult,
	DeleteWriteOpResultObject,
	UpdateWriteOpResult,
} from "mongodb";
import { StoreObject } from "../types/StoreObject";

export type GetCollection = () => Promise<Collection>;

export type CrudOperations<DataType extends StoreObject> = {
	create: (data: DataType[]) => Promise<InsertWriteOpResult<DataType>>;
	read: (ids: ObjectID[]) => Promise<DataType[]>;
	update: (data: DataType[]) => Promise<UpdateWriteOpResult[]>;
	delete: (ids: ObjectID[]) => Promise<DeleteWriteOpResultObject>;
};

export const createCrudOperations = <DataType extends StoreObject>(
	getCollection: GetCollection,
): CrudOperations<DataType> => ({
	create: async (data: DataType[]) => {
		const collection = await getCollection();
		return collection.insertMany(data);
	},
	read: async <DataType>(ids: ObjectID[]) => {
		const collection = await getCollection();
		const cursor = collection.find<DataType>({ _id: { $in: ids } });
		return cursor.toArray();
	},
	update: async (data: DataType[]) => {
		const collection = await getCollection();
		return Promise.all(
			data.map(d => collection.updateOne({ _id: d._id }, { $set: d })),
		);
	},
	delete: async (ids: ObjectID[]) => {
		const collection = await getCollection();
		return collection.deleteMany({ _id: { $in: ids } });
	},
});
