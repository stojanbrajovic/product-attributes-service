import { getDb } from "./getDb";
import { Attribute } from "../types/Attribute";
import { createCrudOperations } from "./createCrudOperations";

const ATTRIBUTES_COLLECTION = "ATTRIBUTES";

const getCollection = async () => {
	const db = await getDb();
	return db.collection(ATTRIBUTES_COLLECTION);
};

export const attributeCrud = createCrudOperations<Attribute>(getCollection);
