import { MongoClient } from "mongodb";
import { mongodbUrl, dbName } from "../../config.json";

const client = new MongoClient(mongodbUrl, { useUnifiedTopology: true });
const connectPromise = client.connect();

export const getDb = async () => {
	await connectPromise;
	return client.db(dbName);
};
