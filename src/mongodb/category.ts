import { getDb } from "./getDb";
import Category from "../types/Category";
import { createCrudOperations } from "./createCrudOperations";

const CATEGORY_COLLECTION = "CATEGORY_COLLECTION";

const getCollection = async () => {
	const db = await getDb();
	return db.collection(CATEGORY_COLLECTION);
};

export const categoriesCrud = createCrudOperations<Category>(getCollection);
